___step 1: 
install php-fpm, nginx

sudo nano /etc/php/7.4/fpm/php.ini

cgi.fix_pathinfo=0
short_open_tag = On

___step 2: 
add end in file:
/etc/nginx/sites-available/default

change directory folder: /home/sk/WEB/testjob/testjob/www/public/


server {
    listen 9696;
    server_name _;
    
    client_max_body_size 150m;

    server_tokens off;
    
    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    charset utf-8;

    root /home/sk/WEB/testjob/testjob/www/public/;
    index index.php;
    
    location ~* \.(js|jpg|jpeg|gif|png|css|zip|tgz|gz|rar|bz2|doc|xls|exe|pdf|ppt|tar|wav|bmp|rtf|swf|ico|flv|txt|xml|docx|xlsx|ttf|mp3)$
	{
		index index.html;
		access_log off;
		expires 30d;
	}

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    
    location ~ \.php$ {
        fastcgi_pass unix:/run/php/php7.4-fpm.sock;
        fastcgi_index index.php;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
		include /etc/nginx/fastcgi_params;
    }

}

___step 3:
restart all
/etc/init.d/nginx restart && /etc/init.d/php7.4-fpm restart