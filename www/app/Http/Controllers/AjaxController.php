<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Users;

class AjaxController extends Controller
{
    public function upload_file(Request $request)
    {
        if( isset($_FILES['file']) )
        {
            if( $_FILES['file']['type'] == 'text/csv' )
            {
                Users::truncate();
                
                // ++ SAVE CSV
                
                if (($handle = fopen($_FILES['file']['tmp_name'], "r")) !== FALSE) {
    
                    $row_num = 0;
                    
                    while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
                        $num_colums = count($data);
                        
                        $row_num++;
        
                        if( $row_num == 1 ) {
                            continue;
                        }

                        if( $num_colums != 10 ) {
                            continue;
                        }

                        $active      = $data[0] == 'Y' ? 1 : 0;
                        $first_name  = $data[1];
                        $last_name   = $data[2];
                        $email       = $data[3];
                        $xml_id      = $data[4];
                        $gender      = $data[5] == 'M' ? 1 : 0;

                        list($day, $month, $year) = explode('.', $data[6]);
                        
                        

                        $date = \DateTime::createFromFormat('d.m.Y', $data[6]);


                        $birthday    = $date->format('Y-m-d');
                        $w_position  = $data[7];
                        $region      = $data[8];
                        $city        = $data[9];

                        
                        $v = new Users();
                        $v->active      = $active;
                        $v->first_name  =  $first_name;
                        $v->last_name   =  $last_name;
                        $v->email       =  $email;
                        $v->xml_id      =  $xml_id;
                        $v->gender      =  $gender;
                        $v->birthday       =  $birthday;
                        $v->work_position  =  $w_position;
                        $v->region         =  $region;
                        $v->city           =  $city;

                        $v->save();
                    }
                    
                    fclose($handle);
                }
                
                // -- SAVE CSV
                
                unlink($_FILES['file']['tmp_name']);
                
                return response()->json([
                    'status'=> 'ok'
                ]);
            }
        }
        
        
    }
}
