// onkeypress="g_jquery_limit_textarea(this, 127)"
function g_jquery_limit_textarea(obj, count_chars)
{
  if($(obj).val().length > count_chars)
  {
      $(obj).val($(obj).val().substr(0, count_chars));
  }
}

// Global center
(function( $ ){
  jQuery.fn.center = function (callback) {
      this.css("position","absolute");
      
      var _top_calc = (($(window).height() - this.outerHeight()) / 3);
      
      if (_top_calc < 0)
      {
        _top_calc = 0;
      }
      
      this.css("top", _top_calc + $(window).scrollTop() + "px");
      this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
      
      if( callback != undefined ) { setTimeout(function(){  callback();  }, 100); }
      
      return this;
  }
})( jQuery );

(function( $ ){
  jQuery.fn.center1 = function (callback) {
      this.css("position","absolute");
      
      var _top_calc = (($(window).height() - this.outerHeight()) / 3);
      
      if (_top_calc < 0)
      {
        _top_calc = 0;
      }
      
      this.css("top", _top_calc + $(window).scrollTop() + "px");
      this.css("left", (($(window).width() - this.outerWidth()) / 2) + $(window).scrollLeft() + "px");
      
      if( callback != undefined ) { setTimeout(function(){  callback();  }, 100); }
      
      return this;
  }
})( jQuery );

(function( $ ){
  jQuery.fn.center_form_obj = function (obj) {
    
      var offset = $(obj).offset();
      this.css("position","absolute");
      this.css("top", (($(obj).height() - this.outerHeight()) / 2) + offset.top + "px");
      this.css("left", (($(obj).width() - this.outerWidth()) / 2) + offset.left + "px");
      return this;
  }
})( jQuery );


function g_AjaxGetErrorMessage(x, exception, error)
{
    var message;
    var statusErrorMap = {
        '400' : "Сервер понял запрос, но содержимое запроса было недопустимым.",
        '401' : "Не авторизованный доступ.",
        '403' : "Запрещенный ресурс недоступен.",
        '500' : "Внутренняя ошибка сервера.",
        '503' : "Сервис недоступен."
    };
    
    if (x.status) 
    {
        message = statusErrorMap[x.status];
        
        if( ! message )
        {
            message = "Произошла неизвестная ошибка.";
        }
    }
    else if(exception == 'parsererror')
    {
        message = "Ошибка: не удалось выполнить синтаксический анализ запроса JSON.";
    }
    else if(exception == 'timeout')
    {
        message = "Время ожидания истекло.";
    }
    else if(exception == 'abort')
    {
        message = "Запрос был прерван сервером.";
    }
    else 
    {
        message = "Произошла неизвестная ошибка.";
    }
    
    return message;
}