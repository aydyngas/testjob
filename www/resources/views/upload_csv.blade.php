<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>Загрузить файл с профилями</title>
        <link href = "/static/normalize.css" rel = "stylesheet" />
        <link href = "/static/reset.css" rel = "stylesheet" />
        <link href = "/static/main.css" rel = "stylesheet" />

        <script src="/static/jquery.min.js"></script>
        <script src="/static/global_function_jquery.js"></script>
        
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            body {
                padding: 15px;
            }
            
            #error_upload { color: red; display: block; margin: 15px 0; }
        </style>
        <script>
            
            function showWait(text)
            {
                $('#info_progress_txt').html( text == undefined ? "Обработка..." : text );
                $('#info_progress').center1();
                $('#fade').show();
                $('#info_progress').show();
            }

            function hideWait()
            {
                $('#fade').hide();
                $('#info_progress').hide();
            }
            
            function upload_file(obj, parameters, test_function, callback_ok, callback_error, callback_onprogress)
            {
                var xhr = new XMLHttpRequest();
                xhr.upload.onprogress = function (event)
                {
                    if(callback_onprogress) { callback_onprogress(event); }
                }

                var f_name = $.trim($(obj).get(0).files[0].name);

                if( test_function && ! test_function( f_name.toLowerCase() ) ) { return; }

                showWait("Загрузка файла: " + f_name + "...")

                var file = $(obj)[0];

                var fd = new FormData();
                fd.append("file", file.files[0]);
                fd.append("cmd", "upload_file");

                if(typeof parameters === 'object') {
                    for(var k in parameters) {
                        fd.append(k, parameters[k]);
                    }
                }

                xhr.onload = xhr.onerror = function ()
                {
                    hideWait();

                    if (this.status == 200)
                    {
                        var res = JSON.parse(this.responseText);

                        if (res.status == "ok") { if (callback_ok) { callback_ok(res); } }
                    } 
                    else
                    {
                        if (callback_error) { callback_error(this) };
                    }
                };
                
                xhr.open("POST", "/ajax/upload_file/", false);
                xhr.setRequestHeader('X-CSRF-TOKEN', $('meta[name="csrf-token"]').attr('content') );
                xhr.send(fd);
            }
            
            $(function(){
                
                
                
                $('#upload_btn').on('click', function()
                {
                    upload_file(
                        $('#file_upload')
                      , { }
                      , function(f_name)
                      {
                          if (f_name.length == 0 || !/\.csv$/i.test(f_name))
                          {
                              $('#error_upload').html('Неверный формат. Доступные форматы: csv');
                              return false;
                          }

                          return true;
                      }
                      , function (res)
                      {
                          $('#error_upload').html('Данные успешно загружены');
                      }
                      , function (res)
                      {
                          if(res.status != 200)
                          {
                              $('#error_upload').html('Неизвестная ошибка');
                          }
                      });
                });
                
            });
        </script>
    </head>
    <body>
        
        <div>
            <input id="file_upload" type="file">
            <span id="upload_btn" class="btn">Загрузить CVS</span>
        </div>
        <div>
            <div id="error_upload"></div>
        </div>
        
        
        <!-- HIDDEN -->
        <div id="fade"></div>
        <div id='info_progress'>
            <img class="rotation_image" src="/images/ajax-loader.gif"> <span id='info_progress_txt'>Авторизация...</span>
        </div>
    </body>
</html>
