<?php
ini_set('display_startup_errors', 1);
ini_set('display_errors',   1);
mb_http_input('utf-8');
mb_http_output('utf-8');

function getDsnMysql($db_name, $host='localhost', $port=null, $charset = null)
{
    $dns = 'mysql:host='.$host.';dbname='.$db_name.';';

    if(! empty($port))
    { $dns .= 'port='.$port.';'; }

    if(! empty($charset))
    { $dns .= 'charset='.$charset.';'; }

    return $dns;
}

function synchonizeTimeZonePhpMysql()
{
  $now = new DateTime();  
  $mins = $now->getOffset() / 60;

  $sgn = ($mins < 0 ? -1 : 1);  
  $mins = abs($mins);  
  $hrs = floor($mins / 60);  
  $mins -= $hrs * 60;

  return sprintf('%+d:%02d', $hrs*$sgn, $mins);
}

$db = new PDO(
                  getDsnMysql('testjob', 'localhost', '3306', 'utf8mb4')
                , 'root'
                , 'zaqwsx12'
            );

var_dump($db);

if (($handle = fopen('../[_TMP_]/import.csv', "r")) !== FALSE) {
    
    $row_num = 0;
    
    while (($data = fgetcsv($handle, 1000, ';')) !== FALSE) {
        $num_colums = count($data);
        
        $row_num++;
        
        if( $row_num == 1 ) {
            continue;
        }
        
        if( $num_colums != 10 ) {
            continue;
        }
        
        
        
        $active      = $data[0] == 'Y' ? 1 : 0;
        $first_name  = $data[1];
        $last_name   = $data[2];
        $email       = $data[3];
        $xml_id      = $data[4];
        $gender      = $data[5] == 'M' ? 1 : 0;
        
        list($day, $month, $year) = explode('.', $data[6]);
        
        $date = \DateTime::createFromFormat('d.m.Y', $data[6]);
        
        $birthday    = $date->format('Y-m-d');
        $w_position  = $data[7];
        $region      = $data[8];
        $city        = $data[9];
        
        print_r( [ 
                 'active' => $active
                , 'first_name' => $first_name
                , 'last_name' => $last_name
                , 'email' => $email
                , 'xml_id' => $xml_id
                , 'gender' => $gender
                , 'birthday' => $birthday
                , 'position' => $w_position
                , 'region' => $region
                , 'city' => $city
                ] );
        
        $query = 'INSERT INTO `users`(`id`, `first_name`, `last_name`, `gender`, `email`, `birthday`, `work_position`, `region`, `city`, `xml_id`, `active`) '
                . 'VALUES ('
                . ' NULL'
                . ','.$db->quote($first_name)
                . ','.$db->quote($last_name)
                . ','.(int) $gender
                . ','.$db->quote($email)
                . ','.$db->quote($birthday)
                . ','.$db->quote($w_position)
                . ','.$db->quote($region)
                . ','.$db->quote($city)
                . ','.$db->quote($xml_id)
                . ','.(int) $active
                . ')';
        
        var_dump($query);
        
        $db->exec($query);
    }
    
    fclose($handle);
}