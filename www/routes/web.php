<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

define('G_DS', DIRECTORY_SEPARATOR);
define('G_DIR_TMP', public_path() .G_DS. 'tmp' . G_DS);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/load-profiles/', function () {
    
    return view('upload_csv');
    
});

Route::group(['prefix' => '/ajax/', 'middleware' => \App\Http\Middleware\VerifyCsrfToken::class], function()
{
    Route::post('upload_file', 'AjaxController@upload_file' );
    
    
});

Route::get('/list/{sort}/{page}/{count}/', function ($sort, $page, $count) {
    
    $sort = strtolower($sort) == 'asc' ? 'asc' : 'desc';
    $page = (int) $page;
    $count = (int) $count;
    $offset = $page * $count;
    
    $result = App\Users::orderBy('first_name', $sort)
            ->orderBy('last_name', $sort)
            ->offset($offset)
            ->limit($count)
            ->get();
    
    $result = $result->toArray();
    
    return response()->json([
                    $result
                ]);
});