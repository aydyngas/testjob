--
-- Table structure for table `post_comments`
--

CREATE TABLE `post_comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `parent_comment_id` int(11) NOT NULL COMMENT 'if = 0 is root level',
  `text_comment` longtext NOT NULL,
  `time_add` int(11) NOT NULL COMMENT 'unix timestamp',
  `time edit` int(11) NOT NULL COMMENT 'unix timestamp',
  `status` int(11) NOT NULL COMMENT '1 show; 0 hide'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post_comments`
--

INSERT INTO `post_comments` (`id`, `post_id`, `uid`, `parent_comment_id`, `text_comment`, `time_add`, `time edit`, `status`) VALUES
(1, 1, 5, 0, 'ghhjghj ', 22, 22, 1),
(2, 1, 5, 1, 'fgfgh gfh', 55, 55, 1),
(3, 1, 5, 1, 'jkllhjkl ', 11, 11, 1);

-- количетсво комментариев к посту

SELECT a.text_comment as TextComment,
             a.id as ParentID,
             b.TotalCout
  FROM post_comments a INNER JOIN
         (
        SELECT parent_comment_id, COUNT(1) as TotalCout
          FROM post_comments
           WHERE parent_comment_id <> id
        GROUP BY parent_comment_id
         ) b 
     ON a.id = b.parent_comment_id
     AND b.TotalCout>0

WHERE id =1

----------------------------------

-- иерархический вывод 

DROP PROCEDURE IF EXISTS getpath;
DELIMITER $$
CREATE PROCEDURE getpath(IN p_id INT, OUT path TEXT)
BEGIN
    DECLARE catname VARCHAR(20);
    DECLARE temppath TEXT;
    DECLARE tempparent INT;
    SET max_sp_recursion_depth = 255;
    SELECT text_comment, parent_comment_id FROM post_comments WHERE id=p_id INTO catname, tempparent;
    IF tempparent IS NULL
    THEN
        SET path = catname;
    ELSE
        CALL getpath(tempparent, temppath);
        SET path = CONCAT(p_id, '/', tempparent);
    END IF;
END$$
DELIMITER ;

DROP FUNCTION IF EXISTS getpath;
DELIMITER $$
CREATE FUNCTION getpath(p_id INT) RETURNS TEXT DETERMINISTIC
BEGIN
    DECLARE res TEXT;
    CALL getpath(p_id, res);
    RETURN res;
END$$
DELIMITER ;


SELECT id, post_comments.text_comment, getpath(id) AS path FROM post_comments

id	text_comment	path	
1	ghhjghj 	1/0	
2	fgfgh gfh	2/1	
3	jkllhjkl 	3/1	



